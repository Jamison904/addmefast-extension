chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.notification === 'captcha') {
        captchaNotification(request);
        sendResponse({action: 'captcha solved'});
    }
    
    if (request.notification === 'update') {
        checkForUpdate(function (requiresUpdate) {
            sendResponse({
                action: 'check for update',
                requiresUpdate: requiresUpdate,
                message: 'This bot is out of date. Download latest version from ' + chrome.runtime.getManifest()['homepage_url']
            });
        });
    }

    return true;
});

function randomString(stringSize) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var index = 0; index < stringSize; index++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function captchaNotification(request) {    
    chrome.notifications.create(request.settings, function (notificationId) {
        setTimeout(function () {
            chrome.notifications.clear(notificationId);
        }, request.closeAfter);
    });

    chrome.notifications.onClicked.addListener(function (notificationId) {
        chrome.notifications.clear(notificationId);
    });
}

function checkForUpdate(callback) {
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "http://tbn.mcrasty.com/versions.php", true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var serverVersion = JSON.parse(xhr.responseText);
            var manifestFile = chrome.runtime.getManifest();
            var requiresUpdate = true;

            if (manifestFile['version'] === serverVersion.addmefast) {
                requiresUpdate = false;
            }
            callback(requiresUpdate);
        }
    }
    xhr.send();
}